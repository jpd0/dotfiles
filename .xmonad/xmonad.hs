--      _ ____
--     | |  _ \
--  _  | | | | |
-- | |_| | |_| |
--  \___/|____/  xmonad.hs
--
--------------------
-- IMPORTS ---------
--------------------
import XMonad
import Data.Monoid
import System.Exit

import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import XMonad.Layout.LayoutModifier
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops

import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Run

import qualified XMonad.StackSet as W
import qualified Data.Map        as M


--------------------
-- VARIABLES -------
--------------------
myTerminal      = "urxvt"
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myClickJustFocuses :: Bool
myClickJustFocuses = False
myBorderWidth   = 2
myModMask       = mod4Mask
--myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]
myWorkspaces    = [" I "," II "," III "," IV "," V "," VI "," VII "," VIII "," IX "]
myNormalBorderColor  = "#282c34"
myFocusedBorderColor = "#61afef"

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset


--------------------
-- STARTUP ---------
--------------------
myStartupHook = do 
  spawnOnce "nitrogen --restore &"
  spawnOnce "picom &"
  spawnOnce "lxpolkit &"
  spawnOnce "unclutter --timeout 3 &"
  spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282c34  --height 20 &"
  spawnOnce "dunst &"
  spawnOnce "$HOME/.xmonad/tray-programs.sh &"


--------------------
-- BINDS -----------
--------------------
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm,               xK_Return), spawn $ XMonad.terminal conf) -- launch a terminal
    , ((modm,               xK_p     ), spawn "dmenu_run -fn 'Ubuntu:size=11:weight=Bold' -p 'run:' -nb '#282c34' -nf '#bbc2cf' -sb '#61afef' -sf '#000000'") -- launch dmenu
    , ((modm,               xK_e     ), spawn "thunar") -- launch file manager
    , ((modm,               xK_v     ), spawn "virtualbox --style Fusion") -- launch vbox  
    , ((modm,               xK_w     ), spawn "firefox") -- launch firefox
    , ((modm,               xK_x     ), spawn "$HOME/.xmonad/edit-config.sh") -- edit xmonad config
    , ((modm,               xK_Escape), spawn "killall clearine; clearine") -- launch logout ui
    , ((modm .|. shiftMask, xK_l     ), spawn "i3lock -c 282c34") -- lock with i3-lock
    , ((modm .|. shiftMask, xK_c     ), kill)-- close focused window
    , ((modm,               xK_space ), sendMessage NextLayout) -- Rotate through the available layout algorithms
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf) --  Reset the layouts on the current workspace to default
    , ((modm,               xK_Tab   ), windows W.focusDown) -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown) -- Move focus to the next window
    , ((modm,               xK_k     ), windows W.focusUp  ) -- Move focus to the previous window
    , ((modm,               xK_m     ), windows W.focusMaster  ) -- Move focus to the master window
    , ((modm .|. shiftMask, xK_Return), windows W.swapMaster) -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  ) -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    ) -- Swap the focused window with the previous window
    , ((modm,               xK_h     ), sendMessage Shrink) -- Shrink the master area
    , ((modm,               xK_l     ), sendMessage Expand) -- Expand the master area
    , ((modm,               xK_t     ), withFocused $ windows . W.sink) -- Push window back into tiling
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1)) -- Increment the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1))) -- Deincrement the number of windows in the master area
    , ((modm              , xK_b     ), sendMessage ToggleStruts) -- Toggle the status bar gap
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess)) -- Quit xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; killall xmobar; xmonad --restart") -- Restart xmonad
    ]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9] -- mod-[1..9], Switch to workspace N
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]] -- mod-shift-[1..9], Move client to workspace N

----------------------------------

myKeys1 =
        [ ("M-f", sendMessage (Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
        , ("M-n", sendMessage (Toggle NOBORDERS)) -- Toggles noborder 
        ]



--------------------
-- MOUSE BINDS -----
--------------------
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w -- mod-button1, Set the window to floating mode and move by dragging
                                       >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w -- mod-button3, Set the window to floating mode and resize by dragging
                                       >> windows W.shiftMaster))
    ]


--------------------
-- LAYOUTS ---------
--------------------
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw True (Border i i i i) True (Border i i i i) True

myLayout = avoidStruts $ smartBorders $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) $ mySpacing 4 (tiled ||| Full)
  where
     tiled   = Tall nmaster delta ratio -- default tiling algorithm partitions the screen into two panes
     nmaster = 1 -- The default number of windows in the master pane
     ratio   = 1/2 -- Default proportion of screen occupied by master pane
     delta   = 3/100 -- Percent of screen to increment by when resizing panes


--------------------
-- WINDOWS ---------
--------------------
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , className =? "Steam"          --> doFloat
    , className =? "VirtualBox"     --> doFloat
    , title =? "Oracle VM VirtualBox Manager"   --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
    ]


--------------------
-- MISC HOOKS ------
--------------------
myEventHook = mempty
myLogHook = return ()


--------------------
-- MAIN ------------
--------------------
main = do
  xmproc <- spawnPipe "xmobar $HOME/.config/xmobar/xmobarrc"
  xmonad $ ewmh def
    {   terminal           = myTerminal
      , focusFollowsMouse  = myFocusFollowsMouse
      , clickJustFocuses   = myClickJustFocuses
      , borderWidth        = myBorderWidth
      , modMask            = myModMask
      , workspaces         = myWorkspaces
      , normalBorderColor  = myNormalBorderColor
      , focusedBorderColor = myFocusedBorderColor

      , keys               = myKeys
      , mouseBindings      = myMouseBindings

      , layoutHook         = myLayout
      , manageHook         = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks
      , handleEventHook    = myEventHook <+> docksEventHook <+> fullscreenEventHook
      , startupHook        = myStartupHook
      , logHook = myLogHook <+> dynamicLogWithPP xmobarPP
      			{ ppOutput = hPutStrLn xmproc
                        , ppCurrent = xmobarColor "#98c379" "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#98c379" ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor "#61afef" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#c678dd" ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#b3afc2" "" . shorten 60     -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> <fn=2>|</fn> </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor "#e06c75" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
			}
        } `additionalKeysP` myKeys1

