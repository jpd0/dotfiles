#!/bin/bash

export EDITOR="nvim"
export VISUAL="nvim"

HISTSIZE= HISTFILESIZE= # Infinite history

alias ls="lsd --group-dirs first"
alias vim="nvim"
alias nf="neofetch"

#export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
#export PS1="\[$(tput bold)\]\[$(tput setaf 4)\][\u@\h \W]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
#eval "$(starship init bash)"

