
nitrogen --restore
xss-lock slock &
#light-locker &
slstatus &
xdg-xmenu > $HOME/.config/xmenu/menu &
lxpolkit &
xfce4-power-manager &
dunst &
xcompmgr -Cn &
unclutter -idle 3 &

#SYSTRAY
clipit &
sleep 0.2s
cbatticon -n -x xfce4-power-manager-settings &
sleep 0.2s
nm-applet &
sleep 0.2s
volumeicon &
#expressvpn-gui-gtk &
